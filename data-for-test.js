var testName = 'Тест по какому-то разделу'
var quesAr =[
    {
        translate: 'ВОПРОС 1',
        answers: {
            answer_1: {
                traslate: 'ответ 1'
            },
            answer_2: {
                traslate: 'ответ 2'
            },
            answer_3: {
                traslate: 'ответ 3'
            },
        },
        rightAns: [ 2 ]			
    },
    {
        translate: 'ВОПРОС 2',
        answers: {
            answer_1: {
                traslate: 'ответ 2.1 Четвертый сезон полюбившегося многими зрителями сериала по вселенной DC. "Стрела" - это история жизни и борьбы Оливера Куина, который провел порядка пяти лет на необитаемом острове с одной целью -выжить и помочь своему родному городу. Оливер осознал весь тот ужас, который происходит на улицах, где он вырос и получил образование.'
            },
            answer_2: {
                traslate: 'ответ 2.2 Очередной сезон полюбившегося зрителям великолепного юмористического сериала "Теория большого взрыва". Настоящая легенда современного телевидения, которая уже в одиннадцатый раз возвращается на экраны вашего ТВ. Перед вами старая-добрая знакомая история о простых юных гениях, которые пытаются познать глубины науки и, параллельно, устроить свою личную жизнь.'
            },
            answer_3: {
                traslate: 'ответ 2.2.1 Одиннадцатый сезон настоящей классики британского телевидения. Перед вами сериал, который стал символом столетия! Проект "Доктор Кто" - это продолжительная фантастическая история, тянущаяся с середины прошлого века. До сих пор сериал выпускают и делают его современнее и интереснее. В каждом новом сезоне приключения Доктора'
            },
            answer_4: {
                traslate: 'ответ 2.3 Даже повелителю Преисподней становится иногда скучно! Люцифер Морнингстар начал впадать в уныние и захотел внести разнообразие в свое существование! Он оставляет Преисподнюю, чтобы отправиться в Лос – Анджелес и стать управляющим…собственного ночного клуба! В клубе под названием «Lux» царит безудержное веселье. Обаятельный Люцифер приковывает женское внимание, к нему невозможно остаться равнодушным, он хорош собой и привлекателен. '
            },
        },
        rightAns: [ 2 , 4 ]
    },
    
    {
        translate: 'ВОПРОС 3',
        answers: {
            answer_1: {
                traslate: 'ответ 3.1'
            },
            answer_2: {
                traslate: 'ответ 3.2'
            },
            answer_3: {
                traslate: 'ответ 3.2.1'
            },
            answer_4: {
                traslate: 'ответ 3.3'
            },
            answer_5: {
                traslate: 'ответ 5.3'
            },
            answer_6: {
                traslate: 'ответ 6.3'
            },
            answer_7: {
                traslate: 'ответ 7.3'
            },
        },
        rightAns: [ 1 , 2 , 3]
    },
    {
        translate: 'ВОПРОС 1',
        answers: {
            answer_1: {
                traslate: 'ответ 1'
            },
            answer_2: {
                traslate: 'ответ 2'
            },
            answer_3: {
                traslate: 'ответ 3'
            },
        },
        rightAns: [ 2 ]			
    },
    {
        translate: 'ВОПРОС 2',
        answers: {
            answer_1: {
                traslate: 'ответ 2.1 Четвертый сезон полюбившегося многими зрителями сериала по вселенной DC. "Стрела" - это история жизни и борьбы Оливера Куина, который провел порядка пяти лет на необитаемом острове с одной целью -выжить и помочь своему родному городу. Оливер осознал весь тот ужас, который происходит на улицах, где он вырос и получил образование.'
            },
            answer_2: {
                traslate: 'ответ 2.2 Очередной сезон полюбившегося зрителям великолепного юмористического сериала "Теория большого взрыва". Настоящая легенда современного телевидения, которая уже в одиннадцатый раз возвращается на экраны вашего ТВ. Перед вами старая-добрая знакомая история о простых юных гениях, которые пытаются познать глубины науки и, параллельно, устроить свою личную жизнь.'
            },
            answer_3: {
                traslate: 'ответ 2.2.1 Одиннадцатый сезон настоящей классики британского телевидения. Перед вами сериал, который стал символом столетия! Проект "Доктор Кто" - это продолжительная фантастическая история, тянущаяся с середины прошлого века. До сих пор сериал выпускают и делают его современнее и интереснее. В каждом новом сезоне приключения Доктора'
            },
            answer_4: {
                traslate: 'ответ 2.3 Даже повелителю Преисподней становится иногда скучно! Люцифер Морнингстар начал впадать в уныние и захотел внести разнообразие в свое существование! Он оставляет Преисподнюю, чтобы отправиться в Лос – Анджелес и стать управляющим…собственного ночного клуба! В клубе под названием «Lux» царит безудержное веселье. Обаятельный Люцифер приковывает женское внимание, к нему невозможно остаться равнодушным, он хорош собой и привлекателен. '
            },
        },
        rightAns: [ 2 , 4 ]
    },
    
    {
        translate: 'ВОПРОС 3',
        answers: {
            answer_1: {
                traslate: 'ответ 3.1'
            },
            answer_2: {
                traslate: 'ответ 3.2'
            },
            answer_3: {
                traslate: 'ответ 3.2.1'
            },
            answer_4: {
                traslate: 'ответ 3.3'
            },
            answer_5: {
                traslate: 'ответ 5.3'
            },
            answer_6: {
                traslate: 'ответ 6.3'
            },
            answer_7: {
                traslate: 'ответ 7.3'
            },
        },
        rightAns: [ 1 , 2 , 3]
    },
    {
        translate: 'ВОПРОС 1',
        answers: {
            answer_1: {
                traslate: 'ответ 1'
            },
            answer_2: {
                traslate: 'ответ 2'
            },
            answer_3: {
                traslate: 'ответ 3'
            },
        },
        rightAns: [ 2 ]			
    },
    {
        translate: 'ВОПРОС 2',
        answers: {
            answer_1: {
                traslate: 'ответ 2.1 Четвертый сезон полюбившегося многими зрителями сериала по вселенной DC. "Стрела" - это история жизни и борьбы Оливера Куина, который провел порядка пяти лет на необитаемом острове с одной целью -выжить и помочь своему родному городу. Оливер осознал весь тот ужас, который происходит на улицах, где он вырос и получил образование.'
            },
            answer_2: {
                traslate: 'ответ 2.2 Очередной сезон полюбившегося зрителям великолепного юмористического сериала "Теория большого взрыва". Настоящая легенда современного телевидения, которая уже в одиннадцатый раз возвращается на экраны вашего ТВ. Перед вами старая-добрая знакомая история о простых юных гениях, которые пытаются познать глубины науки и, параллельно, устроить свою личную жизнь.'
            },
            answer_3: {
                traslate: 'ответ 2.2.1 Одиннадцатый сезон настоящей классики британского телевидения. Перед вами сериал, который стал символом столетия! Проект "Доктор Кто" - это продолжительная фантастическая история, тянущаяся с середины прошлого века. До сих пор сериал выпускают и делают его современнее и интереснее. В каждом новом сезоне приключения Доктора'
            },
            answer_4: {
                traslate: 'ответ 2.3 Даже повелителю Преисподней становится иногда скучно! Люцифер Морнингстар начал впадать в уныние и захотел внести разнообразие в свое существование! Он оставляет Преисподнюю, чтобы отправиться в Лос – Анджелес и стать управляющим…собственного ночного клуба! В клубе под названием «Lux» царит безудержное веселье. Обаятельный Люцифер приковывает женское внимание, к нему невозможно остаться равнодушным, он хорош собой и привлекателен. '
            },
        },
        rightAns: [ 2 , 4 ]
    },
    
    {
        translate: 'ВОПРОС 3',
        answers: {
            answer_1: {
                traslate: 'ответ 3.1'
            },
            answer_2: {
                traslate: 'ответ 3.2'
            },
            answer_3: {
                traslate: 'ответ 3.2.1'
            },
            answer_4: {
                traslate: 'ответ 3.3'
            },
            answer_5: {
                traslate: 'ответ 5.3'
            },
            answer_6: {
                traslate: 'ответ 6.3'
            },
            answer_7: {
                traslate: 'ответ 7.3'
            },
        },
        rightAns: [ 1 , 2 , 3]
    }
];
		
		