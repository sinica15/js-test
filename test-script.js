appendForm('#form-placeholer', quesAr);

//		checkForm($('#form-placeholer form'));

function createForm(quesArry) {
    var form = '<span></span>';
    form +='<p class="testName">' + testName + '</p>';
    form +='<p>пр.от. 1.2  2.2,4  3.1,2,3</p>';
    
    form +='<form id="questions-form">';

    for (let ques_number=0; ques_number < quesArry.length; ques_number++ ){
        form+='<div class="one-question"><span class="question-title">' + quesArry[ques_number]['translate'] + '</span>';
        var multiAns = false;
        if((quesArry[ques_number]['rightAns'].length >= 2)){
            for (answer_number in quesArry[ques_number]['answers']){
            var quesAnsID='q' + ques_number + answer_number;
            var input ='<div class="answer-choices"><input class="my-answers" type="checkbox" name="ques' + ques_number + '" value="'+ answer_number + '" id="' + quesAnsID + '">';
            var label ='<label for="' + quesAnsID + '">' + quesArry[ques_number]['answers'][answer_number]['traslate'] + '</label></div>';
            form+= input;
            form+= label;
            }
        } else {
            for (answer_number in quesArry[ques_number]['answers']){
                var quesAnsID='q' + ques_number + answer_number;
                var input ='<div class="answer-choices"><input class="my-answers" type="radio" name="ques' + ques_number + '" value="'+ answer_number + '" id="' + quesAnsID + '">';
                var label ='<label for="' + quesAnsID + '">' + quesArry[ques_number]['answers'][answer_number]['traslate'] + '</label></div>';
                form+= input;
                form+= label;
            }
        }

        form +='</div>';
    }
    form += '<div class="ready-button-container"><input type="button" value="Готово!" id="ready-button"></div>'
    form += '</form>';

    return form;
}
function appendForm(selector, obj) {
    $(selector).append(createForm(obj));
}
function collectUserAnswersOnOneQuestion(Arr, i){
//            console.log(i);
    for (answer_number in quesAr[i]['answers']){
        var hitSel = "#q"+ i + answer_number +":checked";
//                console.log(hitSel);
        if ($(hitSel).val() !== undefined){
            Arr.push($(hitSel).val());
        }
    }
//            console.log(Arr);
}
function a2ArrComparisonForTest(ArrAns, ArrRightAns){
    var result = 0;
    var foundAns = 0;



    for (var i = 0; i < ArrRightAns.length; i++){
        if (ArrAns.indexOf(ArrRightAns[i]) >= 0){
            foundAns++;
        }
    }
    var flagDebug = false;

//            console.log('пользователь (' + ArrAns + ')');
//            console.log('правельные (' + ArrRightAns + ')');
    if(flagDebug){
        console.log('найдено правельных ответов   ' + foundAns);
        console.log('длина пользователь           ' + ArrAns.length);
        console.log('длина правельные             ' + ArrRightAns.length);        
    }


    if (ArrAns.length == 0) {
        if (flagDebug) {console.log('flag0');}
        return 0;
    }
    if (foundAns == 0) {
        if (flagDebug) {console.log('flag1');}
        return 0;
    }
    if (foundAns == ArrRightAns.length && ArrAns.length == ArrRightAns.length) {
        if (flagDebug) {console.log('flag2');}
        return 2;
    }
    if (foundAns == ArrRightAns.length && ArrAns.length <= (ArrRightAns.length + 1 )) {
        if (flagDebug) {console.log('flag3');}
        return 1;
    }
    if (foundAns == (ArrRightAns.length - 1 ) && ArrAns.length <= (ArrRightAns.length + 1 )) {
        if (flagDebug) {console.log('flag4');}
        return 1;
    }
    if (flagDebug) {console.log('flag5');}
    return 0;
}
function addAnsWordToArr(Arr){
    copyArr = Arr.slice();
    for(var i = 0; i < Arr.length; i++){
        copyArr[i] = 'answer_' + Arr[i];
    }
    return copyArr;
}

function hightLightAns(i){
    var userAnswers = [];
    var oneQuestionRightAnswers = addAnsWordToArr(quesAr[i]['rightAns']);
    collectUserAnswersOnOneQuestion(userAnswers, i);
    for (var j=0; j<userAnswers.length; j++){
        var selector = '#q'+ i + userAnswers[j];
        
//        console.log("<<<=====");
//        console.log("жопа");
//        console.log(oneQuestionRightAnswers);
//        console.log(userAnswers[j]);
//        console.log(oneQuestionRightAnswers.indexOf(userAnswers[j]) >= 0);
//        console.log("=====>>>");
        
        if(oneQuestionRightAnswers.indexOf(userAnswers[j]) >= 0){
//            console.log("селектор вопроса: " + selector);
            $(selector).parent().addClass("true-ans");
        }else{
            console.log("селектор вопроса: " + selector);
            $(selector).parent().addClass("false-ans");
        }
    }
}

function toChekOneAnswer(i){
    var oneQuestionUserAnswers = [];            
    var oneQuestionRightAnswers = addAnsWordToArr(quesAr[i]['rightAns']);
//            console.log(i);
    collectUserAnswersOnOneQuestion(oneQuestionUserAnswers, i);
    return a2ArrComparisonForTest(oneQuestionUserAnswers, oneQuestionRightAnswers);
}

$(document).ready(function(){
    $("#ready-button").click(function(){
        console.clear();
        var rAns = 0;
        var qustionQuantity = 0;               
        for(var i = 0; i < quesAr.length; i++, qustionQuantity++){
            console.log( i + ' вопрос' );
            hightLightAns(i);
            rAns += toChekOneAnswer(i);
        }
//        console.log(ansAr);
        console.log('');
        console.log('счет '+ rAns + ' ' + qustionQuantity*2);

        var score = rAns/(qustionQuantity*2)*100;
//        alert(score);
        console.log(Math.round(score));
        $("#ready-button").remove();
        $(".my-answers").attr("disabled", "true");
        $("#form-placeholer").append("<div class='result-score'>Ваш результат: " + Math.round(score) + " из 100</div>");
//        alert(Math.round(score));
    });
});